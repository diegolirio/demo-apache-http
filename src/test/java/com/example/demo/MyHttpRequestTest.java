package com.example.demo;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Test;

public class MyHttpRequestTest {
	
	@Test
    public void request() throws ClientProtocolException, IOException {

		StatusLine statusline = mock(StatusLine.class);
		CloseableHttpResponse response = mock(CloseableHttpResponse.class);
		CloseableHttpClient httpClient = mock(CloseableHttpClient.class);
		
		when(statusline .getStatusCode()).thenReturn(200);
		when(response.getStatusLine()).thenReturn(statusline);
			    
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream("{\"message\":\"success\"}".getBytes());
		when(response.getEntity()).thenReturn(new InputStreamEntity(byteArrayInputStream));
		//Header[] mockedHeaders = new Header[] { new BasicHeader("headerA", "valueA") };		
		//when(response.getAllHeaders()).thenReturn(mockedHeaders);
		when(httpClient.execute(any(HttpPost.class))).thenReturn(response);
		
		new MyHttpRequest(httpClient).execute("sdff5dh");
	    
    }	
	
	@Test
	public void test2() throws ClientProtocolException, IOException {
		
		CloseableHttpResponse httpResponse = mock(CloseableHttpResponse.class);
	    StatusLine statusLine = mock(StatusLine.class);
	    CloseableHttpClient closeableHttpClient = mock(CloseableHttpClient.class);
	    
	    when(statusLine.getStatusCode()).thenReturn(200);
	    when(httpResponse.getStatusLine()).thenReturn(statusLine);
	    
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream("{\"message\":\"success\"}".getBytes());
		when(httpResponse.getEntity()).thenReturn(new InputStreamEntity(byteArrayInputStream));	    
	    
		when(closeableHttpClient.execute(any(HttpPost.class))).thenReturn(httpResponse);
		
	    new MyHttpRequest(closeableHttpClient).execute("sdff5dh");
	
	}
	
}
