package com.example.demo;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ServiceTest {

	@InjectMocks
	Service service;
	
	@Mock private StatusLine statusLine;
	@Mock private CloseableHttpResponse httpResponse;
	@Mock private CloseableHttpClient client;

	private ByteArrayInputStream byteArrayInputStream;
	
	@Before
	public void before() {
		byteArrayInputStream = new ByteArrayInputStream("{\"message\":\"success\"}".getBytes());		
	}
	
	@Test
	public void test() throws ClientProtocolException, IOException {
	    when(statusLine.getStatusCode()).thenReturn(200);
	    when(httpResponse.getStatusLine()).thenReturn(statusLine);	    
		when(httpResponse.getEntity()).thenReturn(new InputStreamEntity(byteArrayInputStream));	    
		when(client.execute(any(HttpPost.class))).thenReturn(httpResponse);
		boolean request = service.request();
		assertTrue(request);
	}
	
}
