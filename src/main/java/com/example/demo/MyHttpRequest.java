package com.example.demo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class MyHttpRequest {

	private CloseableHttpClient client;

	public MyHttpRequest(CloseableHttpClient client) {
		this.client = client;
	}
	
	public CloseableHttpResponse execute(String param) {
		try {
			HttpPost httpPost = new HttpPost("http://www.examplesafsfg.com");
		 
		    List<NameValuePair> params = new ArrayList<NameValuePair>();
		    params.add(new BasicNameValuePair("username", "John"));
		    params.add(new BasicNameValuePair("password", "pass"));
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			
		    CloseableHttpResponse response = client.execute(httpPost);
		    
		    System.out.println(response);
		    
		    StatusLine statusLine = response.getStatusLine();
		    
		    System.out.println(statusLine.getStatusCode());
		    
		    client.close();    	
		    
		    return response;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		throw new RuntimeException("ERRO");
	 
	}
	
}
