package com.example.demo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class Service {
	
	@Autowired
	private CloseableHttpClient client;
	
    public boolean request() throws ClientProtocolException, IOException {
	    
	    HttpPost httpPost = new HttpPost("http://www.examplesafsfg.com");
	 
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("username", "John"));
	    params.add(new BasicNameValuePair("password", "pass"));
	    httpPost.setEntity(new UrlEncodedFormEntity(params));
	 
	    CloseableHttpResponse response = client.execute(httpPost);
	    
	    System.out.println(response);
	    
	    StatusLine statusLine = response.getStatusLine();
	    
	    System.out.println(statusLine.getStatusCode());
	    
	    //assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
	    client.close();    	
	    
	    return response.getStatusLine().getStatusCode() == 200;
    }	

}
